package exec;

import java.io.IOException;
import java.util.*;

import beans.*;

public class Run {

	private static Scanner sc;

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.out.println("Welcome on Internet Of Thing platform.");
		sc = new Scanner(System.in);
		int choix=-1;
		
		// creation des objets
		Thing t1 = new Thing("f0:de:f1:39:7f:17","1");
		Thing t2 = new Thing("f0:de:f1:39:7f:18","2");
		Thing thTempo = new ThingTempo("f0:de:f1:39:7f:18","3", 3000);
		
		//creation des services
		Service s1 = new Service("mon_service1");
		Service s2 = new Service("mon_service2");
		SmartHome sh = new SmartHome("myKWHome");
		QuantifiedSelf qs = new QuantifiedSelf("RUNstats");
		
		//souscription des objets au service
		t1.subscribe(s1);
		t1.subscribe(s2);
		t2.subscribe(s1);
		

		//creation des dataReceiver
		KeyboardInput k = new KeyboardInput();
		FileReader f = new FileReader("simu.txt");
		k.open();
		f.open();
		
		//initialisation du plateforme
		Platform p = new Platform();
		p.addService(s1);
		p.addService(s2);
		p.addThing(t1);
		p.addThing(t2);
		
		
		while(choix == -1){
			System.out.println("1:Version 1 du projet");
			System.out.println("2:Version 2 du projet");
			System.out.println("3:Version 3 du projet");
			choix = sc.nextInt();
			switch(choix){
				case 1:
					p.run(k);
					break;
					
				case 2:
					p.run(f);
					break;
					
				case 3:
					
					t1.subscribe(sh);
					t1.subscribe(qs);
					
					t2.subscribe(sh);
					t2.subscribe(qs);
					
					thTempo.subscribe(s1);
					thTempo.subscribe(s1);
					thTempo.subscribe(sh);
					thTempo.subscribe(qs);
					
					p.addThing(thTempo);
					p.addService(sh);
					p.addService(qs);
					p.run(k);
					break;
					
				default:
					System.err.print("Choix non disponible");
					choix = -1;
					break;
			}
		}
		
		System.out.println("Bye.");
	}

}
