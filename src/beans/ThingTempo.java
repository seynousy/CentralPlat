package beans;

import java.util.Date;

public class ThingTempo extends Thing{
	private long delay;
	private long lastUpdate;
	public ThingTempo(String mac, String id, long delay) {
		super(mac, id);
		// TODO Auto-generated constructor stub
		this.delay = delay;
		this.lastUpdate = 0;
	}
	public void update(){
		Date now = new Date() ;
	    long time = now.getTime() ;
	    if((time-lastUpdate)>delay){
	    	super.update();
	    }
	}

}
